# -*- coding: utf-8 -*-

from odoo import api, fields, models
import logging
_logger = logging.getLogger(__name__)


class ViewGanttBa(models.Model):
    _inherit = 'ir.ui.view'
    
    type = fields.Selection(selection_add=[('ganttview', 'GanttView')])

class ViewGanttProjectTak(models.Model):
    _inherit = 'project.task'
    
    gantt_start_date = fields.Datetime(string='Planned start', index=True, copy=False)
    gantt_stop_date = fields.Datetime(string='Planned stop', index=True, copy=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        """ Set the correct label for `unit_amount`, depending on company UoM """
        result = super(ViewGanttProjectTak, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        return result